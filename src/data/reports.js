/**
 * reports.js is responsible for manipulating the reports collection
 * in the Mongo database. In architecture parlance, it is a Data Access Object.
 * It abstracts away the details of interact with the database.
 */
const Database = require("../lib/database.js");
const logger = require("../lib/logger");

class Reports {

    static async existsInDB(date) {
        try {
          const reportsCollection = await getReportsCollection();
          let report = await reportsCollection.findOne({ date: date }, { projection: { _id: 0 } });
          console.log(report !== null);
          return report !== null;
        } catch (e) {
          logger.error("ReportAccessObject.existsInDB", e);
          throw {
            code: 500,
            error: "Internal Server Error",
            caused_by: e,
          };
        }
      }
    
    static async getAll() {
        const reportsCollection = await getReportsCollection();
        const cursor = reportsCollection.find({ }, {projection:{ _id: 0 }});
        let reports = await cursor.toArray();
        return reports;
    }

    static async create(reportDate) {
        const inventoryCollection = await getInventoryCollection();
        const reportsCollection = await getReportsCollection();
        let date = reportDate.date

        let inventory = inventoryCollection.find({});
        const data = [];
        for await (const item of inventory) {
            let upc = item.upc;
            let name = item.name;
            let quantity = item.quantity;
            data.push([upc, name, quantity]);
        }
        let report = await reportsCollection.insertOne( { data:data, date:date } );
        let final = await reportsCollection.findOne({_id:report.insertedId}, { projection: { _id: 0 } });
        return final;
    }

    static async getOne(date) {
        const reportsCollection = await getReportsCollection();
        let report = await reportsCollection.findOne({date: date}, { projection: { _id: 0 } });
        return report;
    }
    static async deleteOne(date) {
        const reportsCollection = await getReportsCollection();
        const result = await reportsCollection.deleteOne(
          { date: date }
        );
        return result.deletedCount >= 1;
      }
      static async deleteAll() {
        const reportsCollection = await getReportsCollection();
        const result = await reportsCollection.deleteMany();
        return result.deletedCount >= 1;
      }
    }

    
async function getReportsCollection() {
    const database = await Database.get();
    return database.db("reports").collection("reports");
  }
async function getInventoryCollection() {
    const database = await Database.get();
    return database.db("inventory").collection("inventory");
}

    //module.exports = Entries;
    //module.exports = Inventory;
    module.exports = Reports;