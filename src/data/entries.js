/**
 * entries.js is responsible for manipulating the entries collection
 * in the Mongo database. In architecture parlance, it is a Data Access Object.
 * It abstracts away the details of interact with the database.
 */

const Database = require("../lib/database.js");
const logger = require("../lib/logger");

class Entries {
    static async getAll() {
        const entriesCollection = await getentriesCollection();
        const cursor = entriesCollection.find({ }, {projection:{ _id: 0 }});
        let logs = await cursor.toArray();
        return logs;
    }

    static async getOne(sessionID) {
        const entriesCollection = await getentriesCollection();
        let entry = await entriesCollection.findOne({sessionID: sessionID}, { projection: { _id: 0 } });
        return entry;
    }

    static async create(entryData) {
        const entriesCollection = await getentriesCollection();
        const result = await entriesCollection.insertOne(entryData);
        let entry = await entriesCollection.findOne({_id: result.insertedId}, { projection: { _id: 0 } });
        return entry;
    }

    static async deleteOne(sessionID) {
        const entriesCollection = await getentriesCollection();
        const result = await entriesCollection.deleteOne(
          { sessionID: sessionID }
        );
        return result.deletedCount >= 1;
      }

    static async sessionInDB(sessionID) {
        try {
          const entriesCollection = await getentriesCollection();
          let session = await entriesCollection.findOne({ sessionID: sessionID }, { projection: { _id: 0 } });
          console.log(session !== null);
          return session !== null;
        } catch (e) {
          logger.error("SessionAccessObject.existsInDB", e);
          throw {
            code: 500,
            error: "Internal Server Error",
            caused_by: e,
          };
        }
    }
}

async function getentriesCollection() {
    const database = await Database.get();
    return database.db("entries").collection("entries");
}

module.exports = Entries;
