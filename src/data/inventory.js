/**
 * inventories.js is responsible for manipulating the items collection in the
 * Mongo database. In architecture parlance, it is a Data Access Object.
 * It abstracts away the details of interact with the database.
 */
const Database = require("../lib/database.js");
const logger = require("../lib/logger");

class Inventory {

  static async itemInInventory(upc) {
    try {
      const reportsCollection = await getInventoryCollection();
      let item = await reportsCollection.findOne({ upc: upc }, { projection: { _id: 0 } });
      console.log(item !== null);
      return item !== null;
    } catch (e) {
      logger.error("ItemAccessObject.existsInDB", e);
      throw {
        code: 500,
        error: "Internal Server Error",
        caused_by: e,
      };
    }
  }

  static async getAll() {
    const inventoryCollection = await getInventoryCollection();
    const inventory_cursor = inventoryCollection.find({ }, {projection:{ _id: 0 }});
    let inventory = await inventory_cursor.toArray();
    return inventory;
  }

  static async getOne(upc) {
    const inventoryCollection = await getInventoryCollection();
    let inventory = await inventoryCollection.findOne({ upc: upc }, { projection: { _id: 0 } });
    return inventory;
  }

  static async create(itemData) {
    const inventoryCollection = await getInventoryCollection();
    const result = await inventoryCollection.updateOne(
      { upc: itemData.upc },
      { $set: { name: itemData.name, quantity: itemData.quantity } },
      { upsert: true }
    );
    console.log(result);
    let inventory = await inventoryCollection.findOne({ upc: itemData.upc }, { projection: { _id: 0 } });
    return inventory;
  }

  static async updateItem(item) {
    const inventoryCollection = await getInventoryCollection();
    const result = await inventoryCollection.findOneAndUpdate( {upc: item.upc}, {$set: { name: item.name}, $inc: { quantity: item.quantity}}, {upsert: true, returnNewDocument: true} );
    console.log(result);
    if (await inventoryCollection.findOne( {upc: item.upc, quantity: {$lt: 1}} )) {
      await inventoryCollection.deleteOne( {upc:item.upc} );
  }
  }

  // Runs when posting entry
  static async updateAll(sessionID) {
    const inventoryCollection = await getInventoryCollection();
    inventoryCollection.deleteMany();
    const entriesCollection = await getEntriesCollection();

    let sessions = entriesCollection.find({ sessionID: sessionID });
    const data = new Map();
    for await (const myDoc of sessions) {
      for (let entry of myDoc.entries) {
        let upc = entry.upc;
        let name = entry.name;
        let quantity = entry.quantity;
        if (data.has(upc)) {
          data.set(upc, [name, (data.get(upc)[1] + quantity)]);
        }
        else {
          data.set(upc,[name, quantity]);
        }
      }
    }
    for (let [key,val] of data) {
      await inventoryCollection.insertOne( { upc:key, name:val[0], quantity:val[1] } );
    }
    
    const cursor = inventoryCollection.find({ }, {projection:{ _id: 0 }});
    let inventory = await cursor.toArray();
    return inventory;
}

  static async deleteOne(upc) {
    const inventoryCollection = await getInventoryCollection();
    const result = await inventoryCollection.deleteOne(
      { upc: upc }
    );
    return result.deletedCount >= 1;
  }
}

async function getInventoryCollection() {
  const database = await Database.get();
  return database.db("inventory").collection("inventory");
}

async function getEntriesCollection() {
  const database = await Database.get();
  return database.db("entries").collection("entries");
}


module.exports = Inventory;
