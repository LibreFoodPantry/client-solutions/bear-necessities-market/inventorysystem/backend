const { SERVER_PORT, OPENAPI_FILE } = require('./lib/config.js');
const cors = require('cors');
const express = require('express');
const http = require('http');
const logger = require('./lib/logger.js');
const OpenApiValidator = require('express-openapi-validator');
const mountEndpoints = require('./lib/mount-endpoints.js');
const MessageBroker = require('./lib/messageBroker');
const Database = require('./lib/database');
const scheduleAPICall = require('./lib/scheduler');

async function main() {
  let app = await buildApp();
  let server = http.createServer(app)
  server.listen(SERVER_PORT);
  scheduleAPICall();
}

async function buildApp() {
  const app = express();
  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(
    OpenApiValidator.middleware({
      apiSpec: OPENAPI_FILE,
      validateRequests: true,
      validateResponses: false,
    }),
  );

  mountEndpoints(app);

  app.use((err, req, res, next) => {
    if (res.headersSent) {
      return next(err);
    }

    logger.error(__filename, err);

    res.status(err.status || 500).json({
      message: err.message,
      errors: err.errors,
    });
  });

  return app;
}

const getServerStatus = () => {
  // wait to check status becasue rabbitmq
  // takes several seconds to load
  setTimeout(async () => {
    let mongoConnected = false, rabbitConnected = false;
    await Database.get()
      .then(() => mongoConnected = true)
      .catch((err) => logger.info(err))
    await MessageBroker.getChannel()
      .then(() => rabbitConnected = true)
      .catch(err => logger.info(err))
    logger.info({
      "Server Running On": config.HOST_BASE_URL,
      "DB Connected": mongoConnected,
      "Rabbit Connected": rabbitConnected
    })
  }, config.TIME_OUT)
}

main()
  .then(getServerStatus)
  .catch(e => logger.info(e))
