const Reports = require("../data/reports.js");

module.exports = {
    method: 'delete',
    path: '/reports',
    async handler(request, response) {
        const isDeleteSuccessful = await Reports.deleteAll();
        response.status(200).json(isDeleteSuccessful);
    }
};