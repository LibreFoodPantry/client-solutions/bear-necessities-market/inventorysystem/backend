const Reports = require("../data/reports.js");

module.exports = {
    method: 'get',
    path: '/reports',
    async handler(request, response) {
        const reports = await Reports.getAll();
        response.status(200).json(reports);
    }
};
