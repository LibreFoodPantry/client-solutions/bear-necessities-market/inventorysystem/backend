const Entries = require("../data/entries.js");

module.exports = {
    method: 'delete',
    path: '/entries/:sessionID',
    async handler(request, response) {
        const sessionID = request.params.sessionID;
        const item = await Entries.getOne(sessionID);
        const isDeleteSuccessful = await Entries.deleteOne(sessionID);
        if (isDeleteSuccessful) {
            response.status(200).json(item);
        } else {
            response.status(404).json({
                status: 404,
                error: "Not Found",
                message: "Entry record missing for requested sessionID"
            });
        }
    }
};
