const Reports = require("../data/reports.js");

module.exports = {
    method: 'delete',
    path: '/reports/:date',
    async handler(request, response) {
        const date = request.params.date;
        const report = await Reports.getOne(date);
        const isDeleteSuccessful = await Reports.deleteOne(date);
        if (isDeleteSuccessful) {
            response.status(200).json(report);
        } else {
            response.status(404).json({
                status: 404,
                error: "Not Found",
                message: "Report record missing for requested date"
            });
        }
    }
};
