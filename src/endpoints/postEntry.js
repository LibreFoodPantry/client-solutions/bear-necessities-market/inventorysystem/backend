const Entries = require("../data/entries.js");
const Inventory = require("../data/inventory.js");

module.exports = {
    method: 'post',
    path: '/entries',
    async handler(request, response) {
        const entryData = request.body;
        const entry = await Entries.create(entryData);
        const inventory = Inventory.updateAll(request.body.sessionID);
        const resourceUri = `${request.originalUrl}/${entry.sessionID}`;
        response.status(201).location(resourceUri).json(entry);
    }
};
