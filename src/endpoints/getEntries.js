const Entries = require("../data/entries.js");

module.exports = {
    method: 'get',
    path: '/entries',
    async handler(request, response) {
        const entries = await Entries.getAll();
        response.status(200).json(entries);
    }
};