const Reports = require("../data/reports.js");

module.exports = {
    method: 'post',
    path: '/reports',
    async handler(request, response) {
        const reportDate = request.body;
        if ( await Reports.existsInDB(request.body.date) === true ) {
            response.status(409).json({
                status: 409,
                error: "Conflict",
                message: "Report already exists"
              });
        }
        else {
            const report = await Reports.create(reportDate);
            const resourceUri = `${request.originalUrl}/${report.date}`;
            response.status(201).location(resourceUri).json(report);
        }
    }
};
