const Inventory = require("../data/inventory.js");

module.exports = {
    method: 'get',
    path: '/inventory/:upc',
    async handler(request, response) {
        const upc = request.params.upc;
        const item = await Inventory.getOne(upc);
        if (item !== null) {
            response.status(200).json(item);
        } else {
            response.status(404).json({
                status: 404,
                error: "Not Found",
                message: "Item record missing for requested upc"
            });
        }
    }
};
