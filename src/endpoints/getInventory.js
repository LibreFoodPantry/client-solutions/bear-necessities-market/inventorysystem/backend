const Inventory = require("../data/inventory.js");

module.exports = {
    method: 'get',
    path: '/inventory',
    async handler(request, response) {
        const inventory = await Inventory.getAll();
        response.status(200).json(inventory);
    }
};
