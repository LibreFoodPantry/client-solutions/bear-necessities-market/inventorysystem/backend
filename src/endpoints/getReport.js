const Reports = require("../data/reports.js");

module.exports = {
    method: 'get',
    path: '/reports/:date',
    async handler(request, response) {
        const date = request.params.date;
        const report = await Reports.getOne(date);
        if (report !== null) {
            response.status(200).json(report);
        } else {
            response.status(404).json({
                status: 404,
                error: "Not Found",
                message: "Report record missing for requested date"
            });
        }
    }
};