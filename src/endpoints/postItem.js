const Inventory = require("../data/inventory.js");

module.exports = {
    method: 'post',
    path: '/inventory',
    async handler(request, response) {
        const itemData = request.body;
        const item = await Inventory.create(itemData);
        const resourceUri = `${request.originalUrl}/${item.upc}`;
        response.status(201).location(resourceUri).json(item);
    }
};
