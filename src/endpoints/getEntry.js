const Entries = require("../data/entries.js");

module.exports = {
    method: 'get',
    path: '/entries/:upc',
    async handler(request, response) {
        const upc = request.params.upc;
        const entry = await Entries.getOne(upc);
        if (entry !== null) {
            response.status(200).json(entry);
        } else {
            response.status(404).json({
                status: 404,
                error: "Not Found",
                message: "Entry records missing for requested upc"
            });
        }
    }
};