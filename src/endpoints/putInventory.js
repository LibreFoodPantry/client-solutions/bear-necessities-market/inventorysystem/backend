const Inventory = require("../data/inventory.js");

module.exports = {
    method: 'put',
    path: '/inventory',
    async handler(request, response) {
        const item = request.body;
        const result = Inventory.updateItem(item);
        response.status(200).json(item);
    }
};
