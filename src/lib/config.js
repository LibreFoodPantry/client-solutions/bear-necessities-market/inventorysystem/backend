const path = require('path');

const ROOT_DIR = path.join(__dirname, "..", "..");

const config = {
  // The port to access this app from the host.
  HOST_BASE_URL: process.env.HOST_BASE_URL ? process.env.HOST_BASE_URL : "http://localhost:10350",
  SERVER_PORT: process.env.SERVER_PORT || 10350,
  
  // MongoDB connection string
  MONGO_URI: process.env.MONGO_URI ? process.env.MONGO_URI : 'mongodb://localhost:27018',
  
  
  ROOT_DIR: ROOT_DIR,
  OPENAPI_FILE: path.join(ROOT_DIR, "lib", "openapi.yaml"),
  ENDPOINTS_DIR: path.join(ROOT_DIR, "src", "endpoints"),

  // RabbitMQ connection string
  RABBITMQ_URL: process.env.RABBITMQ_URL ? process.env.RABBITMQ_URL : 'amqp://guest:guest@localhost:5672/?connection_attempts=5&retry_delay=5',
  //RabbitMQ timeout
  TIME_OUT: process.env.TIME_OUT || 7000,

  API_VERSION: "0.0.1"
};

module.exports = config;
