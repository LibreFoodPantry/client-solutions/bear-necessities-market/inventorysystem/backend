// Use to run an API post request at 11:59 PM every day. It's script dependent. 

const cron = require('node-cron');
const axios = require('axios');

async function makePostRequest() {
    try {
        const today = new Date();
        const formatter = new Intl.DateTimeFormat('en-US', { day: '2-digit', month: '2-digit', year: 'numeric'});
        const sessionID = formatter.format(today).replace(/\//g, '-');;
        const response = await axios.post('http://localhost:10350/reports', {
            date:sessionID
        });
        console.log('API POST request successful:', response.data);
    } catch (error) {
      console.error('Error making API POST request:', error.message);
    }
  }
  
  // Define the function to schedule the task
  function scheduleAPICall() {
    // Schedule the task to run at 11:59 PM every day
    cron.schedule('59 23 * * *', () => {
      console.log('Scheduling API POST request...');
      makePostRequest();
    }, {
      scheduled: true,
      timezone: 'America/New_York'
    });
  }
  

  module.exports = scheduleAPICall;