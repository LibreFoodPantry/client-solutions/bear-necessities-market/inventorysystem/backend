#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.."

# Build the Docker test image
docker build \
  -f ./src/chai_tests/test_container/Dockerfile.test \
  -t test-runner \
  .

# Run the tests
docker run --rm test-runner npm test

# remove container
docker rmi test-runner