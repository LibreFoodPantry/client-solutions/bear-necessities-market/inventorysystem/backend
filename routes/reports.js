const express = require('express');
const router = express.Router();
const Report = require('../models/Report');

// POST
router.post('/post', async (req, res) => {
    const report = new Report({
        data: req.body.data,
        date: req.body.date
    });
    try {
        const newReport = await report.save();
        res.status(201).json(newReport);
    } catch (err) {
        res.status(204).json({message: err});
    }
});

// GET
router.get('/get', async (req, res) => {
    try {
        const reports = await Report.find();
        res.status(200).json(reports);
    } catch (err) {
        res.status(404).json({message: err});
    }
});

//GET by date
router.get('/get/:date', async (req, res) => {
    try {
        const specificReport = await Report.findById(req.params.date);
        res.status(200).json(specificReport);
    } catch (err) {
        res.status(404).json({message: err});
    }
});


module.exports = router;
