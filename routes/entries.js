const express = require('express');
const router = express.Router();
const Entry = require('../models/Entry');

// POST
router.post('/post', async (req, res) => {
    const entry = new Item({
        entries: req.body.entries,
        sessionID: req.body.sessionId
    });
    try {
        const newEntry = await entry.save();
        res.status(201).json(newEntry);
    } catch (err) {
        res.status(204).json({message: err});
    }
});

// GET
router.get('/get', async (req, res) => {
    try {
        const entries = await Entry.find();
        res.status(200).json(entries);
    } catch (err) {
        res.status(404).json({message: err});
    }
});

// GET by upc
router.get('/get/:sessionID', async (req, res) => {
    try {
        const specificEntry = await Entry.findById(req.params.sessionID);
        res.status(200).json(specificEntry);
    } catch (err) {
        res.status(404).json({message: err});
    }
});

module.exports = router;
