// API Routes
const express = require('express');
const router = express.Router();
const Item = require('../models/Item');


// POST 

router.post('/post', async (req, res) => {
    const item = new Item({
        upc: req.body.upc,
        name: req.body.name,
        quantity: req.body.quantity
    });
    try{
        const newItem = await item.save();
        res.status(201).json(newItem);
    }catch(err){
        res.status(204).json({message: err});
    }
});

// GET

router.get('/get', async (req, res) => {
    try{
        const items = await Item.find();
        res.status(200).json(items);
    }catch(err){
        res.status(404).json({message:err});
    }
});

// GET item by upc

router.get('/get/:upc', async (req, res) => {
    try{
        const specificItem = await Item.findById(req.params.upc);
        res.status(200).json(specificItem);
    }catch(err){
        res.status(404).json({message:err});
    }
});



// DELETE by upc

router.delete('/delete/:upc', async (req, res) => {
    try{
        const specificItem = await Order.findByIdAndDelete(req.params.upc);
        res.status(200).json(specificItem);
    }catch(err){
        res.status(404).json({message:err});
    }
});

module.exports = router;
