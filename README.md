##### Updated: November 11, 2024

# InventorySystemBackend

## Project Overview

The Backend of the Inventory System for the Bear Necessities Market. This repository contains the code for managing the inventory database, which stores information about items and their quantities. Employees update the inventory through a GUI located in the Frontend.

## Folder Information
* [**.devcontainer**](./.devcontainer/) - Contains the files needed to run the InventorySystemBackend in a dev container.
    * Files: 
        * [**Dockerfile**](./.devcontainer/Dockerfile)
        * [**devcontainer.json**](./.devcontainer/devcontainer.json)

* [**.gitlab**](./.gitlab/) - Provides information on properly committing and merging into the repository.

    * Files: 
        * [**issue_templates**](./.gitlab/issue_templates/)
        * [**merge_request_templates**](./.gitlab/merge_request_templates/)

* [**LICENSES**](./LICENSES/) - Licensing information for InventorySystemBackend

    * Files: 
        * [**CC-BY-SA-4.0.txt**](./LICENSES/CC-BY-SA-4.0.txt)
        * [**GPL-3.0-only.txt**](./LICENSES/GPL-3.0-only.txt)

* [**bin**](./bin/) - Scripts to streamline the running and development of InventorySystemBackend, [Development Commands](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/backend/-/blob/main/docs/developer/development-commands.md) provides further details.

    * Files: 
        * [**build-test-runner.sh**](./bin/build-test-runner.sh)
        * [**build.sh**](./bin/build.sh)
        * [**down.sh**](./bin/down.sh)
        * [**premerge-squash.sh**](./bin/premerge-squash.sh)
        * [**rebuild.sh**](./bin/rebuild.sh)
        * [**restart.sh**](./bin/restart.sh)
        * [**retest.sh**](./bin/retest.sh)
        * [**test.sh**](./bin/test.sh)
        * [**up.sh**](./bin/up.sh)

* [**docs/developer**](./docs/developer/) - Developer documentation covering various aspects of the InventorySystemBackend that will help developers work on the project.
    * Files: 
        * [**automated-testing.md**](./docs/developer/automated-testing.md)
        * [**continuous-integration.md**](./docs/developer/continuous-integration.md)
        * [**dependency-management.md**](./docs/developer/dependency-management.md)
        * [**development-commands.md**](./docs/developer/development-commands.md) 
        * [**development-environment.md**](./docs/developer/development-environment.md)
        * [**documentation-system.md**](./docs/developer/documentation-system.md) 
        * [**linting.md**](./docs/developer/linting.md)
        * [**tools.md**](./docs/developer/tools.md) 
        * [**version-control-system.md**](./docs/developer/version-control-system.md)

* [**lib**](./lib/) - Contains information on API communication and possible errors caused.
    * Files: 
        * [**openapi.yaml**](./lib/openapi.yaml)

* [**models**](./models/) - Attributes of Mongoose Schemas.

    * Files: 
        * [**.gitkeep**](./models/.gitkeep)
        * [**Entry.js**](./models/Entry.js)
        * [**Item.js**](./models/Item.js)
        * [**Report.js**](./models/Report.js)

* [**routes**](./routes/) - HTTP methods for Mongoose Schemas.

    * Files: 
        * [**.gitkeep**](./routes/.gitkeep)
        * [**entries.js**](./routes/entries.js)
        * [**inventory.js**](./routes/inventory.js)
        * [**reports.js**](./routes/reports.js)

* [**src**](./src/) - The code for InventorySystemBackend.

    * Files: 
        * [**data**](./src/data/)
        * [**endpoints**](./src/endpoints/)
        * [**lib**](./src/lib/)
        * [**docker-compose.yaml**](./src/docker-compose.yaml)
        * [**Dockerfile**](./src/Dockerfile)
        * [**index.js**](./src/index.js)
        * [**package-lock.json**](./src/package-lock.json)
        * [**package.json**](./src/package.json)
        * [**yarn.lock**](./src/yarn.lock)

* [**tests**](./tests/) - Automated and manual tests for the InventorySystemBackend.

    * Files: 
        * [**chai_tests**](./tests/chai_tests/)
        * [**manual**](./tests/manual/)
        * [**test-runner**](./tests/test-runner/)

## Status

The Inventory System Backend is complete and working properly with the use of the instructions below. The database is functional, and there are both automated and manual tests implemented. 

## Setting up the Dev Environment

For setup instructions, refer to the detailed guide here: [Install Development Environment](./docs/developer/development-environment.md#install-development-environment).

## Running the Application
Once you have Docker up and running (see [Setting up the Dev Environment](#setting-up-the-dev-environment) above), you can run the following commands in a new terminal.

```bash
# Initial build
$ cd bin
$ sh build.sh

# Intended run of the project (you can also use "docker-compose up" if "docker compose up" doesn't work)
$ cd ../src
$ docker compose up
```

The API implemented by this server is in [lib/openapi.yaml](./lib/openapi.yaml). The API can be viewed:

* Using the Swagger Viewer extension for VS Code which is installed in the Dev Container for this project.
* Directly in the GitLab repository for this project.  

The Backend is hosted on port **10350** and the visualization of the database, which is used for testing purposes, is hosted on port **8080**. To connect to port 8080 the current authorization username and password is "admin" and "pass" respectively.


## Development Infrastructure

The following tools are intended to be used in the backend of the Inventory System:

* [Automated Testing](docs/developer/automated-testing.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Commands](docs/developer/development-commands.md)
* [Setting up Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
* [Linters](docs/developer/linting.md)

For an additional list of tools used in the infrastructure see: [tools.md](docs/developer/tools.md).

## License

[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)
