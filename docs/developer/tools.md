# Tools
These are tools/technologies utilzied in the Inventory System Backend, with direct documentation in docs/developer:
* [Chai](automated-testing.md)
* [Axios](automated-testing.md)
* [npmjs](dependency-management.md)
* [Docker](development-environment.md)
* [VS Code](development-environment.md)
* [Linters](linting.md)
* [Swagger Extension](documentation-system.md)

These are other tools/technologies utilized in the Inventory System Backend, with no direct documentation:
* [MongoDB](https://www.mongodb.com/)
* [Node.js](https://nodejs.org/en/)
* [JavaScript](https://www.javascript.com/)
* [Express](https://expressjs.com/)