# Development Environment - VS Code DevContainer

VS Code DevContainers allows all of your developers to have the same
dependencies, files, and environments. This helps to squash the
"it works on my machine" issue that plagues development teams to this day.
All developers should work within and maintain the project's development
container.

## Install Development Environment

All developers need to install the following
dependencies on their local
computer.

* [Git](https://git-scm.com/)
* [Docker Desktop](https://www.docker.com/)
* [VS Code](https://code.visualstudio.com/)
* [The "Dev Containers" extension for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

### How to Install Development Environment On Different Platforms

  #### Windows
  1. Install Git:
      * Download and install [Git](https://git-scm.com/)
  2. Install Docker Desktop
      * Download [Docker Desktop](https://www.docker.com/)
      * It is highly recommended to ensure that WSL 2 is enabled, when prompted during installation
  3. Install Visual Studio Code
      * Download and install [VS Code](https://code.visualstudio.com/Download)
  4. Install Dev Containers Extension
      * Open VS Code
      * Go to Extensions: ```(Ctrl+Shift+X)```
      * Search for [```Dev Containers```](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) and install the extension

  #### Linux
  1. Install Git:
      * Depending on the Linux Distribution:
        * Debian-based (most common): ```sudo apt update && sudo apt install git```
        * Red Hat-based: ```sudo dnf install git``` or ```sudo yum install git``` for older systems
        * Arch-based: ```sudo pacman -S git```
        * SUSE-based: ```sudo zypper install git```
        * Gentoo-based: ```sudo emerge --ask --verbose dev-vcs/git```
        * Independent (such as Void Linux): ```sudo xbps-install -S git```
  2. Install Docker Desktop
      * Download [Docker Desktop](https://docs.docker.com/desktop/install/linux/)
      * Installation instructions for different Linux distributions:
        * [Install on Ubuntu](https://docs.docker.com/desktop/install/linux/ubuntu/)
        * [Install on Debian](https://docs.docker.com/desktop/install/linux/debian/)
        * [Install on Red Hat Enterprise Linux (RHEL)](https://docs.docker.com/desktop/install/linux/rhel/)
        * [Install on Fedora](https://docs.docker.com/desktop/install/linux/fedora/)
        * [Install on Arch](https://docs.docker.com/desktop/install/linux/archlinux/)
  3. Install Visual Studio Code
      * Download and install [VS Code](https://code.visualstudio.com/Download) for the proper distribution
      * Follow the [official documentation](https://code.visualstudio.com/docs/setup/linux) for a detailed explanation of installing VS Code on Linux
  4. Install Dev Containers Extension
      * Open VS Code
      * Go to Extensions ```(Ctrl+Shift+X)```
      * Search for [```Dev Containers```](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) and install the extension
    
  #### MacOS
  1. Install Git:
      * Git comes with most versions of macOS:
        * Verify the installation: ```git --version```
      * If Git is not installed, install via [Homebrew](https://docs.brew.sh/Installation):
        * ```brew install git```
        * Verify the installation: ```git --version```
  2. Install Docker Desktop
      * Download [Docker Desktop](https://www.docker.com/)
  3. Install Visual Studio Code
      * Download and install [VS Code](https://code.visualstudio.com/Download)
  4. Install Dev Containers Extension
      * Open VS Code
      * Go to Extensions ```(Ctrl+Shift+X)```
      * Search for [```Dev Containers```](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) and install the extension

### Clone, Install, and Run

1. Navigate to this project on GitLab and select
    `Clone -> Open in your IDE -> Visual Studio Code (HTTPS)`. Alternatively you can clone this locally via SSH or HTTPS, but this is the preferred method.
2. Select the location to store the project.
3. Select "Reopen in Container" when option is provided. If the option does not appear, or it disappears, you can select the ```><``` icon in the status bar, and select "Reopen in Container" when prompted.

---

The information below is about how to configure and install
new extension and tools into the development environment.

## Resources

* [VS Code DevContainers](https://code.visualstudio.com/docs/remote/containers)
* [Creating VSCode DevContainers](https://code.visualstudio.com/docs/remote/create-dev-container)

## Files

* .devcontainer/Dockerfile
  * Install additional software through package manager (e.g., apt)
* .devcontainer/devcontainer.json
  * VSCode extensions
  * VSCode settings
  * Hooks to install dependencies

The files above may depend on other files in the project.

## Notes

* Always work inside a devcontainer.
* Avoid using development tools outside the devcontainer, instead install
  them into the devcontainer.
* Install development tools using the above files so that others can use
  them too.
* Rebuild the devcontainer anytime you modify the above files or any file
  they depend on.

## .devcontainer/devcontainer.json

Use this file to

* Install VS Code extensions
* Configure VS Code

This file is configured to call .devcontainer/init_devenv.sh. Use this file to install
additional development dependencies and configure the development environment.

For more information see
  [VS Code: Create DevContainer](https://code.visualstudio.com/docs/remote/create-dev-container)

## .devcontainer/Dockerfile

Read the comments in this file. There are sections that can be uncommented
to install 3rd party tools to the development environment.

For more information see
  [VS Code: Create DevContainer](https://code.visualstudio.com/docs/remote/create-dev-container)

## Working with timezones in VSCode

VSCode sets the default timezone to UTC. After running these
commands to change the timezone, you must reopen container.

To check the available timezones, enter in bash:

```bash
ls /usr/share/zoneinfo
```

To set a new timezone for a single session, enter in bash:

```bash
export TZ="ENTER_TIMEZONE"
```

For example, to set your timezone to ET, we find America/New_York inside
/usr/share/zoneinfo, and then we set TZ as follows.

```bash
export TZ="America/New_York"
```

Confirm it worked by running

```bash
date
```

If you want to have the timezone set every time you open the
container, add the export statement to .bashrc in the home directory
of the user that the container runs as. The following command should work.

```bash
echo 'export TZ="ENTER_TIMEZONE"' >> ~/.bashrc
```

Your TZ should be set the next time you open a new bash shell.

## How we created the initial DevContainer

You should not have to complete this section unless you are building a new
DevContainer for this project or building a DevContainer for your own project.

Here are the steps we took to build the initial DevContainer.  Note: Things
may have changed after we followed these steps. For more information on
how to create a DevContainer see here:
<https://code.visualstudio.com/docs/remote/create-dev-container>

1. Select the Remote Window Indicator (green icon on bottom left of VSCode)
2. Select `Open Folder in Container`
3. After popup, select the folder you want to Containerize
4. Select `Node.js`
5. Select `18` for version
6. Click OK for extensions (Don't select any extensions. Starting the container
  may take a few minutes)
7. You should see a new .devcontainer folder in your repository

**IMPORTANT**: After changing *ANY* files in the `.devcontainer` folder,
you must go back to the remote window indicator and select `Rebuild Container`.
If you don't, your container will **NOT** use your changes.

