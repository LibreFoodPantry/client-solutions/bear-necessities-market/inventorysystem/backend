# Linters

We use multiple tools to lint the parent project's source files.

## Requirements

Docker

## Local CI Usage

See [.gitlab-ci.yml](../../.gitlab-ci.yml)

## Linters List

See [.gitlab-ci.yml](https://gitlab.com/LibreFoodPantry/common-services/tools/pipeline/-/blob/main/source/gitlab-ci/pipeline.yml?ref_type=heads) for a list of the linters available for use.
