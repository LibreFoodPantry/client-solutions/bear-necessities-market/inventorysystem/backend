# Continuous Integration (CI)

CI covers building, linting, and testing

CI aids the developer by automatically building and running
tests to make integration of new code easier.

## Current Pipeline
**Build** - Generates a Docker image of the InventoryBackend server.
* [Documentation](../developer/development-commands.md)

**Lint** - Checks if commit messages follow valid syntax.
* [Documentation](../developer/linting.md)
* [Format](https://www.conventionalcommits.org/en/v1.0.0/)

**Test** - Performs automated testing.
* [Documentation](../developer/automated-testing.md)




[Further information on these tools and more](https://gitlab.com/LibreFoodPantry/common-services/tools). Note that only the tools listed above are included within InventorySystemBackend.

## Related Files

* [.gitlab-ci.yml](../../.gitlab-ci.yml) includes files from the tools/
  that run in the CI/CD pipeline. For more information individual tools,
  see their README.md.

## Official Documentation

* [Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)

