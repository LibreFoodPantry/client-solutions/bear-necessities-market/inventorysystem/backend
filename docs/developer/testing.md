# Automated Testing
Automated testing has not been implemented. The tests currently in the automated testing folder are non functional.
# Testing


## Related files
**Automatic Test Infrastructure** is located in the [chai-tests](../../src/chai_tests) folder. 

Running Automated Tests

- Navigate to InventorySystemBackend
- Press the code button then open in your preferred dev container
    * To learn how to set up dev the container/environment navigate to development-environment.md in GuestInfoBackend
- In main terminal enter: cd bin
- Enter: docker build \
  -f ./src/chai_tests/test_container/Dockerfile.test \
  -t test-runner \
  .
- Enter: run --rm test-runner npm test
- View test results in terminal
- To remove testing container enter: docker rmi test-runner


## Related files

**Manual Tests** are located in the [manual](../../tests/manual) folder.

@@ -12,8 +25,10 @@ Running manual Tests

- Navigate to InventorySystemBackend
- Press the code button then open in your preferred dev container
    * To learn how to set up dev the container/environment navigate to development-environment.md in GuestInfoBackend
- in main terminal enter: cd bin
- Enter: sh build.sh
- Enter: cd ../src
- Then enter: docker compose up
- Navigate to the calls in the manual folder in the testing folder.


## Correct Order of Tests

In order to test and observe the correct behavior of some tests inside of `calls.http`, other requests must be sent before them. Here are the following requests where this is the case.

### 1. GET and DELETE `/inventory/{012345678912}`

For both of these requests to run as expected, the POST request shown below must be first sent.

```http
### Test Create a new item and add it to the BNM inventory.
POST http://localhost:10350/inventory
content-type: application/json

{
  "upc" : "012345678912",
  "name" : "soup",
  "quantity": 5
}
```

### 2. GET and DELETE `/entries/{02-01-2021}`

For both of these requests to run as expected, the POST request shown below must be first sent.

```### Test add entries
POST http://localhost:10350/entries
content-type: application/json

{
  "entries" : [
              {"upc":"012345678912","name":"soup","quantity":5},
              {"upc":"012325674914","name":"oatmeal","quantity":6},
              {"upc":"015923856193","name":"juice","quantity":3}
              ],
  "sessionID" : "01-01-0101"
}
```

### 3. GET and DELETE `/reports/{02-02-2022}`

For both of these requests to run as expected, the POST request shown below must be first sent.

```###
POST  http://localhost:10350/reports
content-type: application/json

{
  "date" : "02-02-2022"
}
```