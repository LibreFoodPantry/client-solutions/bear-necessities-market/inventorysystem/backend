const mongoose = require('mongoose');

const ReportSchema = mongoose.Schema({
    data: Array,
    date: String
});

module.exports = ReportSchema;