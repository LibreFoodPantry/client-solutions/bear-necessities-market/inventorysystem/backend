const mongoose = require('mongoose');

const EntrySchema = mongoose.Schema({
    upc: String,
    quantity: String,
    date: String
});

module.exports = EntrySchema;