const mongoose = require('mongoose');

const InventorySchema = mongoose.Schema({
    upc: String,
    name: String,
    quantity: Number
});

module.exports = InventorySchema;